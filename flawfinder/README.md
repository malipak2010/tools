# "Finds possible security weaknesses in C/C++ source code"

## Supported tags and respective `Dockerfile` links
* `latest` 
([*flawfinder/Dockerfile*](https://gitlab.com/CinCan/dockerfiles/blob/master/flawfinder/Dockerfile))

## Usage

`$ docker run -v /samples:/samples cincan/flawfinder /samples/sample.c`

## Project homepage

https://github.com/david-a-wheeler/flawfinder

# "Open Source Digital Forensics"

## Supported tags and respective `Dockerfile` links

* `latest` 
([*sleuthkit/Dockerfile*](https://gitlab.com/CinCan/dockerfiles/blob/master/sleuthkit/Dockerfile))

## Usage
```
docker run -it -v $(pwd)/samples:/samples cincan/sleuthkit [tool from sleuthkit]
``` 
## Project homepage

https://www.sleuthkit.org/

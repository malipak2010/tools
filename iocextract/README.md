# "Advanced Indicator of Compromise (IOC) extractor"

## Supported tags and respective `Dockerfile` links

* `latest` 
([*iocextract/Dockerfile*](https://gitlab.com/CinCan/Tools/blob/master/pipelines/dockerfiles/iocextract/Dockerfile))

## Project homepage

https://github.com/InQuest/python-iocextract
